#!/usr/bin/env python3
import os
import sys

import setuptools
from setuptools.command.test import test as TestCommand


requires = [
    # TODO Put dependencies here
]

tests_require = [
    {%- if cookiecutter.use_pytest == 'y' %}
    'pytest',
    'pytest-asyncio',
    {%- endif %}
]

here = os.path.abspath(os.path.dirname(__file__))
{% if cookiecutter.use_pytest == 'y' %}


class PyTest(TestCommand):
    user_options = [('pytest-args=', 'a', "Arguments to pass to pytest")]

    def initialize_options(self):
        TestCommand.initialize_options(self)
        try:
            from multiprocessing import cpu_count
            self.pytest_args = ['-n', str(cpu_count()), '--boxed']
        except (ImportError, NotImplementedError):
            self.pytest_args = ['-n', '1', '--boxed']

    def run_tests(self):
        import pytest
        errno = pytest.main(self.pytest_args)
        sys.exit(errno)
{% endif %}


# 'setup.py publish' shortcut.
if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist bdist_wheel')
    os.system('twine upload dist/*')
    sys.exit()

{%- set license_classifiers = {
    'Apache 2.0': 'License :: OSI Approved :: Apache Software License',
    'MIT license': 'License :: OSI Approved :: MIT License',
    'GNU General Public License v3': 'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
} %}

about = {}
with open(os.path.join(here, 'src', '{{ cookiecutter.repo_name }}', '__init__.py')) as f:
    exec(f.read(), about)

with open('README.md', 'r') as f:
    readme = '\n' + f.read()

setuptools.setup(
    name=about['__title__'],
    version=about['__version__'],
    description=about['__description__'],
    long_description=readme,
    long_description_content_type='text/markdown',
    author=about['__author__'],
    author_email=about['__author_email__'],
    url=about['__url__'],
    download_url=about['__download_url__'],
    license=about['__license__'],
    packages=setuptools.find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    install_requires=requires,
    tests_require=tests_require,
    python_requires='>=3.5',
    cmdclass={'test': PyTest},
    keywords='{{ cookiecutter.repo_name }}',
    entry_points={
        'console_scripts': [  # TODO Put entry points here ],
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Operating System :: POSIX :: Linux",
{%- if cookiecutter.license in license_classifiers %}
        '{{ license_classifiers[cookiecutter.license] }}',
{%- endif %}
        "Natural Language :: English",
        "Environment :: Console",
        "Intended Audience :: End Users/Desktop", ] + [
        ('Programming Language :: Python :: %s' % x) for x in
        '3 3.5 3.6 3.7'.split()
    ],
    zip_safe=False,
)
