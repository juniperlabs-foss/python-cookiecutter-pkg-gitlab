# Python Cookiecutter
Cookiecutter template for Python projects. Saves time by bootstrapping your repository.

## Features
- Choose license
- Create setup.py and source path
- Setup tox environment and Gitlab CI
- Prepare documentation

## Usage
To use the template, you have to install Cookiecutter. Simply use pip for the task:

`pip install cookiecutter`

And then run it to bootstrap your project:

`cookiecutter gl:juniperlabs-foss/python-cookiecutter-pkg-gitlab`

For more info, refer to the [documentation][documentation].

## Contributing
For information on how to contribute to the project, please check the [Contributor's Guide][contributing].

## Contact
[support@juniperlabs.io](mailto:support@juniperlabs.io)

[incoming+juniperlabs-foss/python-cookiecutter-pkg-gitlab@gitlab.com](incoming+juniperlabs-foss/python-cookiecutter-pkg-gitlab@gitlab.com)

## License
MIT License

## Credits

This package was created with [Cookiecutter][cookiecutter] and the [python-cookiecutter-pkg-gitlab][python-cookiecutter-pkg-gitlab] project template.

[contributing]: https://gitlab.com/juniperlabs-foss/python-cookiecutter-pkg-gitlab/blob/master/CONTRIBUTING.md
[cookiecutter]: https://github.com/audreyr/cookiecutter
[documentation]: https://juniperlabs-foss.gitlab.io/python-cookiecutter-pkg-gitlab
[python-cookiecutter-pkg-gitlab]: https://gitlab.com/juniperlabs-foss/python-cookiecutter-pkg-gitlab-pkg-gitlab
